const express = require('express');
const mysql = require('mysql');
const bodyparser = require('body-parser');
const port = process.env.NODE_PORT || 8080;
var app = express();

app.listen(
    port, () => console.log(`Listening on port ${port}..`)
);
app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection(
    {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_ROOT_PASSWORD,
        database: process.env.DB_DATABASE,
        multipleStatements: true
    }
);

mysqlConnection.connect(
    (err)=> {
        if(!err)
            console.log('Connection Established Successfully');
        else
            console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
    }
);

app.get('/', 
    (req, res) => {
        let html = '<div style="text-align: center; padding: 4rem;">';
        html += '<h1>Binar Devops Wave 1</h1>';
        html += '<p>Muh Zia Abdillah Asya</p>';
        html += '<a href="https://dev.binar-zia.online/contacts">Contacts</a> / ';
        html += '<a href="https://dev.binar-zia.online/health">Health</a></div>';
        res.send(html)
    }
)

app.get('/health', 
    (req, res) => {
        res.status(200).send('healthy');
    }
)

app.get('/contacts' , 
    (req, res) => {
        mysqlConnection.query(
            'SELECT * FROM contactdetails', (err, rows, fields) => {
                if (!err) {
                    let html = '<table style="border-collapse: collapse; margin: 3rem; font-size: 0.9em; box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);">';
                    html += '<thead>';
                    html += '<tr style="background-color: #009879; color: #ffffff; text-align: left;">';
                    html += '<th style="padding: 12px 15px;">First Name</th>';
                    html += '<th style="padding: 12px 15px;">Last Name</th>';
                    html += '<th style="padding: 12px 15px;">Email</th>';
                    html += '<th style="padding: 12px 15px;">Phone</th>';
                    html += '</tr>';
                    html += '</thead>';
                    html += '<tbody>';
                    for (const row of rows) {
                        html += '<tr style="border-bottom: 1px solid #dddddd;">';
                        html += '<td style="padding: 12px 15px;">' + row.firstname + '</td>';
                        html += '<td style="padding: 12px 15px;">' + row.lastname + '</td>';
                        html += '<td style="padding: 12px 15px;">' + row.email + '</td>';
                        html += '<td style="padding: 12px 15px;">' + row.phone + '</td>';
                        html += '</tr>';
                    }
                    html += '</tbody>';
                    html += '</table>';
                    res.send(html);
                } else {
                    console.log(err);
                }
            }
        )
    } 
);

app.get('/contacts/:id' , 
    (req, res) => {
        mysqlConnection.query(
            'SELECT * FROM contactdetails WHERE contact_id = ?',[req.params.id], (err, rows, fields) => {
                if (!err)
                res.send(rows);
                else
                console.log(err);
            }
        )
    } 
);

app.post('/contacts', 
    (req, res) => {
        let contact = req.body;
        var sql = "SET @contact_id = ?;SET @firstname = ?;SET @lastname = ?;SET @email = ?;SET @phone = ?; CALL contactAddOrEdit(@contact_id,@firstname,@lastname,@email,@phone);";
        mysqlConnection.query(
            sql, [
                contact.contact_id, 
                contact.firstname, 
                contact.lastname, 
                contact.email, 
                contact.phone
            ], (err, rows, fields) => {
                if (!err)
                    rows.forEach(
                        element => {
                            if(element.constructor == Array)
                                res.send('New Contact ID : '+ element[0].contact_id);
                        }
                    );
                else
                    console.log(err);
            }
        )
    }
);

app.put('/contacts', 
    (req, res) => {
        let contact = req.body;
        var sql = "SET @contact_id = ?;SET @firstname = ?;SET @lastname = ?;SET @email = ?;SET @phone = ?; CALL contactAddOrEdit(@contact_id,@firstname,@lastname,@email,@phone);";
        mysqlConnection.query(
            sql, [
                contact.contact_id, 
                contact.firstname, 
                contact.lastname, 
                contact.email, 
                contact.phone
            ], (err, rows, fields) => {
                if (!err)
                    res.send('Contact ID Details Updated Successfully');
                else
                    console.log(err);
            }
        )
    }
);

app.delete('/contacts/:id', 
    (req, res) => {
        mysqlConnection.query(
            'DELETE FROM contactdetails WHERE contact_id = ?', [req.params.id], (err, rows, fields) => {
                if (!err)
                    res.send('Contact ID deleted successfully.');
                else
                    console.log(err);
            }
        )
    }
);